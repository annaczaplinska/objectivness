package zadanie1;

public class MainZadanie1 {
    public static void main(String[] args) {


        OsobaZadanie1 osoba = new OsobaZadanie1("imie", 25);

        osoba.printYourNameAndAge();

        System.out.println(osoba);
        System.out.println("to samo co:");
        System.out.println(osoba.toString());

        System.out.println("imie: " + osoba.getName() + " wiek: " + osoba.getAge());
    }
}
