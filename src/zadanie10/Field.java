package zadanie10;

public class Field {

    private int n;
    private boolean[][] tablica;

    public Field(int n) {
        this.n = n;
        this.tablica = new boolean[n][n];
    }

    public void printField() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (tablica[i][j] == true) {
                    System.out.print("X");

                } else if (tablica[i][j] == false) {
                    System.out.print("O");
                }
            }
            System.out.println();
        }
    }

    public void checkCell(int x, int y) {
                if (tablica[x][y] == false) {
                    tablica[x][y] = true;
                } else if (tablica[x][y] == true) {
                    System.out.println("Komorka byla juz sprawdzana");
                }
    }
}
