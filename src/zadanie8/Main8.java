package zadanie8;

import java.util.Scanner;

public class Main8 {

    public static void main(String[] args) {

        double a;
        double b;
        double c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Licze pierwiastki rownania kwadratowego :)");
        System.out.println("Podaj wartosc a");
        a = scanner.nextInt();
        System.out.println("Podaj wartosc b");
        b = scanner.nextInt();
        System.out.println("Podaj wartosc c");
        c = scanner.nextInt();

        QuadraticEquation kwadrat = new QuadraticEquation(a,b,c);
        System.out.println("Delta wynosi: ");
        System.out.println(kwadrat.calculateDelta());
        System.out.println("x1 wynosi: ");
        System.out.println(kwadrat.calculateX1());
        System.out.println("x2 wynosi: ");
        System.out.println(kwadrat.calculateX2());
    }
}
