package zadanie8;

public class QuadraticEquation {

    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        double delta = ((b * b) - (4 * a * c));
        return delta;
    }


    public double calculateX1() {
        double delta = calculateDelta();
        if (delta < 0) throw new IllegalArgumentException();
        if (delta == 0) {
            double x1 = -b / (2 * a);
            return x1;
        } else {
            double x0 = (-b - Math.sqrt(calculateDelta())) / (2 * a);
            return x0;
        }
    }

    public double calculateX2() {
        double delta = calculateDelta();
        if (delta < 0) throw new IllegalArgumentException();
        if (delta == 0) throw new IllegalArgumentException();
        else {
            double x2 = (-b + Math.sqrt(calculateDelta())) / (2 * a);
            return x2;
        }
    }
}
