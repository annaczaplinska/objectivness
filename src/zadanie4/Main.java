package zadanie4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe a");
        int a = scanner.nextInt();
        System.out.println("Podaj liczbe b");
        int b=scanner.nextInt();


        Calculator calc = new Calculator();

        System.out.println("Dodawanie: ");
        double wynikDod=calc.addTwoNumbers(a , b);
        System.out.println(wynikDod);

        System.out.println("Odejmowanie: ");
        double wynikOdej=calc.substractTwoNumbers(a, b);
        System.out.println(wynikOdej);

        System.out.println("Mnożenie: ");
        double wynikMnoz=calc.multiplyTwoNumbers(a, b);
        System.out.println(wynikMnoz);

        System.out.println("Dzielenie: ");
        double wynikDziel=calc.divideTwoNumbers(a, b);
        System.out.println(wynikDziel);


    }
}
