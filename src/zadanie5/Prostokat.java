package zadanie5;

public class Prostokat {

    double dluzszaDlugosc;
    double krotszaDlugosc;

    public Prostokat(double dluzszaDlugosc, double krotszaDlugosc) {

        this.dluzszaDlugosc = dluzszaDlugosc;
        this.krotszaDlugosc = krotszaDlugosc;
    }

    public double obliczObwod() {
        System.out.println("Obwod: "+ dluzszaDlugosc *2 + krotszaDlugosc*2);
        return dluzszaDlugosc *2 + krotszaDlugosc*2;
    }

    public double obliczPole() {
        System.out.println("Pole: " + dluzszaDlugosc * krotszaDlugosc);
        return dluzszaDlugosc * krotszaDlugosc;
    }
}
