package zadanie5;

public class Kwadrat {

    double dlugoscKrawedzi;

    public Kwadrat(double dlugoscKrawedzi) {

        this.dlugoscKrawedzi = dlugoscKrawedzi;
    }

    public double obliczObwod() {
        System.out.println("Obwod: "+ dlugoscKrawedzi*4);
        return dlugoscKrawedzi * 4;
    }

    public double obliczPole() {
        System.out.println("Pole: " + dlugoscKrawedzi*dlugoscKrawedzi);
        return dlugoscKrawedzi * dlugoscKrawedzi;
    }

}
