package zadanie5;

public class Kolo {

    double promien;

    public Kolo(double promien) {

        this.promien = promien;
    }

    public double obliczObwod() {
        System.out.println("Obwod: "+ Math.PI*promien*2);
        return Math.PI*promien*2;
    }

    public double obliczPole() {
        System.out.println("Pole: " + Math.PI*promien*promien/2);
        return Math.PI*promien*promien/2;
    }



}
