package zadanie5;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        //kwadrat
        System.out.println("Kwadrat. Podaj dlugosc krawedzi");
        int dlugoscKrawedzi = scanner.nextInt();

        Kwadrat kwadrat = new Kwadrat(dlugoscKrawedzi);
        kwadrat.obliczObwod();
        kwadrat.obliczPole();

        //prostokat
        System.out.println("Prostokat. Podaj dlugosc dluzszej krawedzi");
        double dluzszaDlugosc = scanner.nextInt();
        System.out.println("Podaj dlugosc krotszej krawedzi");
        double krotszaDlugosc = scanner.nextInt();

       Prostokat prostokat = new Prostokat(dluzszaDlugosc, krotszaDlugosc);
       prostokat.obliczObwod();
        prostokat.obliczPole();

        //kolo
        System.out.println("Kolo. Podaj dlugosc promienia");
        int promien = scanner.nextInt();

        Kolo kolo = new Kolo(promien);
        kolo.obliczObwod();
        kolo.obliczPole();




    }
}
