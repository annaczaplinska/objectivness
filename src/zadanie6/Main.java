package zadanie6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        BankAccount konto = new BankAccount(1000000);
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj kwote do wplaty");
        double kwotaPlus = scanner.nextInt();
        konto.addMoney(kwotaPlus);

        System.out.println("Podaj kwote do wyplany");
        double kwotaMinus = scanner.nextInt();
        konto.substractMoney(kwotaMinus);

        System.out.println("Twoj stan konta wynosi:");
        konto.printBankAccountStatus();


    }
}
