package zadanie6;

public class BankAccount {

    private double saldo;

    public BankAccount(double saldo) {
        this.saldo = saldo;
    }

    //dodawanie $$
    public double addMoney(double kwotaPlus) {
        System.out.println("Wpłacam kwote: " + kwotaPlus + " Saldo konta: " + (saldo + kwotaPlus));
        return saldo += kwotaPlus;
    }

    //odejmowanie $$
    public double substractMoney(double kwotaMinus) {
        System.out.println("Wypłacam kwote: " + kwotaMinus + " Saldo konta: " + (saldo - kwotaMinus));
        return (saldo -= kwotaMinus);
    }

    //stan konta $$
    public void printBankAccountStatus() {
        System.out.println("Saldo konta: " + saldo  );
    }
}
