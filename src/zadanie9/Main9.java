package zadanie9;

import java.util.Scanner;

public class Main9 {

    public static void main(String[] args) {

        Car car = new Car( "Audi" ,  5, 100, 105, 70, 200, 2016, 0);


        System.out.println("Nazwa i rocznik samochodu: " + car.getCarName());
        System.out.println("Dodaje pasazera");
        car.addPassenger();
        System.out.println("Ilosc pasazerow: " + car.getPassenger() );
        System.out.println("Dodaje pasazera");
        car.addPassenger();
        System.out.println("Ilosc pasazerow: " + car.getPassenger() );
        System.out.println("Odejmuje pasazera");
        car.removePassenger();
        System.out.println("Ilosc pasazerow: " + car.getPassenger());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Aktualna predkosc: "+ car.getSpeed() + " O ile chcesz przyspieszyc? Podaj predkosc:");
        int predkosc = scanner.nextInt();
        System.out.println("Aktualna predkosc: " + car.speedIncrease(predkosc));







    }
}
