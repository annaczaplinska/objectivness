package zadanie9;

public class Car {


    private String name;
    private int seats;
    private int engine;
    private int konie;
    private int speed;
    private int km;
    private int year;
    private int passenger;

    public Car(String name, int seats, int engine, int konie, int speed, int km, int year, int passenger) {
        this.name = name;
        this.seats = seats;
        this.engine = engine;
        this.konie = konie;
        this.speed = speed;
        this.km = km;
        this.year = year;
        this.passenger = passenger;
    }

    // metoda ktora zwraca marke samochodu i rok produkcji
    public String getCarName(){
        return name + " " + year;
    }

    // metoda ktora dodaje pasazera
    public void addPassenger(){
        passenger++;
    }

    // metoda ktora odejmuje pasazera
    public void removePassenger(){
        passenger--;
    }

    // metoda ktora zwieksza predkosc
    public int speedIncrease(int predkosc) {
        if (passenger >= 1) {
            if (km <= 200) {
                if (speed + predkosc <= 1.2 * km) {
                    speed += predkosc;
                } else {
                    System.out.println("Nie możesz jechac tak szybko..");
                }
            } else if (km>200){
                if (speed +predkosc <= 1*km){
                    speed+=predkosc;
                } else {
                    System.out.println("Nie możesz jechac tak szybko..");
                }
            }
            } else {
        System.out.println("Samochod nie moze jechac sam..");
         }
        return speed;
    }

    // metoda ktora zmniejsza predkosc
    public int speedDecrease(int predkosc){
        if (passenger>=1){
            speed -= predkosc;
        }
        return speed;
    }

    public int getPassenger() {
        return passenger;
    }

    public int getSpeed() {
        return speed;
    }
}
