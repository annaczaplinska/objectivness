package zadanie11;

import java.util.ArrayList;

public class Main2 {
    public static void main(String[] args) {

        ArrayList<User> listaUserow = new ArrayList<>();
        User user= new User("Benek" , "kiki47");
        listaUserow.add(user);

        listaUserow.add(new User("Ania", "hasło"));
        listaUserow.add(new User("Felix", "koteczek"));
        listaUserow.add(new User("Tomek", "mały"));

       // user.wypisz();
        //System.out.println(user.wypisz());
        System.out.println(user.toString());
        listaUserow.wypisz();

    }

    //wypisz Arrayliste
    public static void wypisz( ArrayList<String> listaUserow){
        for (int i=0; i<listaUserow.size(); i++){
            System.out.println("Indeks: " + i + " wartosc " + listaUserow.get(i));
        }
    }
}
