package zadanie11;

import java.util.ArrayList;

public class ArrayListMain {
    public static void main(String[] args) {


        ArrayList<Integer> lista = new ArrayList<>();
        lista.add(12);
        lista.add(13);
        lista.add(15);
        lista.add(98);

        System.out.println("For: suma liczb na liscie:");
        System.out.println(sumaList(lista));
        System.out.println("For each: suma liczba na liscie:");
        System.out.println(sumaLista(lista));
        System.out.println("For: iloczyn liczb na liście:");
        System.out.println(iloczynList(lista));
        System.out.println("For each: iloczyn liczb na liscie");
        System.out.println(iloczynLista(lista));
        System.out.println("For: srednia liczb na liscie: ");
        System.out.println(sredniaList(lista));
        System.out.println("For each: srednia liczb na liscie");
        System.out.println(sredniaLista(lista));

        ArrayList<Boolean> logiczne = new ArrayList<>();
        logiczne.add(true);
        logiczne.add(true);
        logiczne.add(true);
        logiczne.add(true);
        logiczne.add(false);
        logiczne.add(true);
        logiczne.add(false);
        logiczne.add(false);
        logiczne.add(false);

        System.out.println("Negacja wartosci logicznych: ");
        zaprzeczenieBoo(logiczne);
    }

    // for: sumę liczb na liście
    public static int sumaList(ArrayList<Integer> lista) {
        int sum = 0;
        for (int i = 0; i < lista.size(); i++) {
            sum += lista.get(i);
        }
        return sum;
    }

    // for each: sumę liczb na liście
    public static int sumaLista(ArrayList<Integer> lista) {
        int sum = 0;
        for (Integer wartosc : lista) {
            sum += wartosc;
        }
        return sum;
    }

    //for:  iloczyn liczb na liście
    public static int iloczynList(ArrayList<Integer> lista) {
        int ilo = 1;
        for (int i = 0; i < lista.size(); i++) {
            ilo *= lista.get(i);
        }
        return ilo;
    }

    //for each:  iloczyn liczb na liście
    public static int iloczynLista(ArrayList<Integer> lista) {
        int ilo = 1;
        for (Integer wartosc : lista) {
            ilo *= wartosc;
        }
        return ilo;
    }


    //for: średnią liczb na liście
    public static double sredniaList(ArrayList<Integer> lista) {
        int sum = 0;
        for (int i = 0; i < lista.size(); i++) {
            sum += lista.get(i);
        }
        return sum / lista.size();
    }

    //for each: średnią liczb na liście
    public static double sredniaLista(ArrayList<Integer> lista) {
        int sum = 0;
        for (Integer wartosc : lista) {
            sum += wartosc;
        }
        return sum / lista.size();
    }

    //negacja wartosci logicznych
    public static void zaprzeczenieBoo(ArrayList<Boolean> logiczne){
        System.out.println(logiczne);
        for (int i=0; i<logiczne.size(); i++){
            Boolean wartosc = logiczne.get(i);
            logiczne.remove(i);
            logiczne.add(i, !wartosc);
        }
        System.out.println(logiczne);
    }
}
