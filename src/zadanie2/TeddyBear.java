package zadanie2;

public class TeddyBear {

    private String name;

    public TeddyBear (String name){
        this.name = name;
    }

    public void imieMisia(){
        System.out.println("Imię misia: " + name);
    }
}
